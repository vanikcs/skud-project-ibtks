FROM python:3.11.4-slim-bookworm

EXPOSE 8000

RUN set -ex; \
    rm /etc/apt/sources.list.d/* && \ 
    echo "deb http://mirror.yandex.ru/debian bookworm main" > /etc/apt/sources.list && \
    echo "deb http://mirror.yandex.ru/debian-security bookworm-security main" >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install --no-install-recommends -y curl gcc && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log} && \
    pip3 list --format freeze | cut -d = -f 1 | xargs pip3 install \
    --upgrade

ENV TZ="Europe/Moscow"

# Create user for app
ENV APP_USER=appuser
RUN useradd --create-home $APP_USER
RUN usermod -aG dialout appuser
WORKDIR /home/$APP_USER
USER $APP_USER

# Set python env vars
ENV PYTHONUNBUFFERED 1
ENV PYTHONNODEBUGRANGES 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONPATH="/home/$APP_USER:$PYTHONPATH"

# Use venv directly via PATH
ENV VENV_PATH=/home/$APP_USER/.venv/bin
ENV USER_PATH=/home/$APP_USER/.local/bin
ENV PATH="$VENV_PATH:$USER_PATH:$PATH"

COPY requirements.txt ./
RUN pip3 install --user --no-cache-dir -r requirements.txt

COPY app/ app/

CMD python3 app/main.py
EXPOSE 5000