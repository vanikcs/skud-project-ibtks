Flask==3.1.0
Flask_Login==0.6.3
flask_paginate==2024.4.12
flask_sqlalchemy==3.0.3
pyfingerprint==1.5
psycopg2-binary==2.9.10