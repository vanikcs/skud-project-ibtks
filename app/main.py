from pyfingerprint.pyfingerprint import PyFingerprint
from flask import Flask
from flask_login import LoginManager
from werkzeug.security import generate_password_hash

from app.settings import Config
from app.models import db, User
from app.routes import init_routes


app_config = Config()

application = Flask(__name__)
application.config.from_object(app_config)
application.secret_key = app_config.SECRET_KEY

db.init_app(application)
manager = LoginManager()
manager.init_app(application)
manager.login_view = 'login'

@manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

fingerprint = PyFingerprint(app_config.USB_PATH, 57600, 0xFFFFFFFF, 0x00000000)
fingerprint.setSecurityLevel(app_config.FINGERPRINT_SECURITY_LEVEL)
init_routes(application, db, fingerprint)

with application.app_context():
    db.create_all()

    login = db.session.query(User).filter_by(login=app_config.ADMIN_USER).first()
    if not login:
        hash_pwd = generate_password_hash(app_config.ADMIN_PASSWORD)
        new_user = User(login=app_config.ADMIN_USER, password=hash_pwd)
        db.session.add(new_user)
        db.session.commit()

    application.run(
        host=app_config.APP_INTERFACE,
        debug=app_config.DEBUG_MODE
        )
