from os import getenv


class Config:
    SQLALCHEMY_DATABASE_URI: str = getenv('SQLALCHEMY_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS: bool = getenv('SQLALCHEMY_TRACK_MODIFICATIONS')
    JSONIFY_PRETTYPRINT_REGULAR: bool = getenv('JSONIFY_PRETTYPRINT_REGULAR')
    MAX_CONTENT_LENGTH: int = int(getenv('MAX_CONTENT_LENGTH'))
    PER_PAGE: int = int(getenv('PER_PAGE'))
    SECRET_KEY: str = getenv('SECRET')
    APP_INTERFACE: str = getenv('APP_INTERFACE')
    DEBUG_MODE: bool = getenv('DEBUG_MODE')
    USB_PATH: str = getenv('USB_PATH')
    ADMIN_USER: str = getenv('ADMIN_USER')
    ADMIN_PASSWORD: str = getenv('ADMIN_PASSWORD')
    FINGERPRINT_SECURITY_LEVEL: int = int(getenv('FINGERPRINT_SECURITY_LEVEL'))
