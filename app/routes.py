import json
import re
from flask import render_template, redirect, url_for, request, flash, Flask
from flask_login import login_user, login_required, logout_user
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import SQLAlchemyError
from werkzeug.security import check_password_hash, generate_password_hash
from pyfingerprint.pyfingerprint import PyFingerprint, FINGERPRINT_CHARBUFFER1

from app.models import Fullname, FullnameLog, User
from app.helpers import check_image


def init_routes(app: Flask, db: SQLAlchemy, fingerprint: PyFingerprint):
    @app.route('/', methods=['GET'])
    def root_page():
        return redirect(url_for('access_page'))

    @app.route('/get_access', methods=['GET'])
    def access_page():
        return render_template('index.html')

    @app.route('/user_manangement', methods=['GET'])
    @login_required
    def user_management():
        page = request.args.get('page', 1, type=int)
        fullnames = Fullname.query.paginate(page=page, per_page=app.config['PER_PAGE'], error_out=False)

        return render_template('user_management.html', fullnames=fullnames)


    @app.route('/auth_management', methods=['GET'])
    @login_required
    def auth_management():
        page = request.args.get('page', 1, type=int)
        fullnames = FullnameLog.query.paginate(page=page, per_page=app.config['PER_PAGE'], error_out=False)

        return render_template('access_control.html', fullnames=fullnames)


    @app.route("/flush_local_db", methods=['POST'])
    @login_required
    def flush_local_db():
        if fingerprint.clearDatabase():
            flash('Локальное хранилище очищено')
        else:
            flash('Не удалось очистить локальное хранилище')

        return redirect(url_for('user_management'))


    @app.route('/add_user', methods=['POST'])
    @login_required
    def add_user():

        text = request.form['text']

        if not re.fullmatch(r'^[A-Za-z]+ [A-Za-z]+$', text):
            flash('Введите корректное фамилию и имя латиницей (два слова через пробел).')

            return redirect(url_for('user_management'))

        existing_user = db.session.query(Fullname).filter_by(name=text).first()
        if existing_user:
            flash('Пользователь с таким именем уже существует.')

            return redirect(url_for('user_management'))

        if not fingerprint.verifyPassword():
            flash('Сканер отпечатков пальцев не подключен.')

            return redirect(url_for('user_management'))

        fingerprint_data = []
        for i in range(3):

            while not fingerprint.readImage():
                pass

            fingerprint.convertImage(FINGERPRINT_CHARBUFFER1)
            characteristics = fingerprint.downloadCharacteristics(FINGERPRINT_CHARBUFFER1)
            fingerprint_data.append(characteristics)

        try:
            fingerprint_json = json.dumps(fingerprint_data)

            new_user = Fullname(name=text, images=fingerprint_json)
            db.session.add(new_user)
            db.session.commit()

            fingerprint.storeTemplate(charBufferNumber=FINGERPRINT_CHARBUFFER1)

            flash('Пользователь успешно добавлен.')

            return redirect(url_for('user_management'))
        except SQLAlchemyError as e:
            db.session.rollback()
            flash('Ошибка при добавлении пользователя: ' + str(e))

            return redirect(url_for('user_management'))


    @app.route('/auth_user', methods=['POST'])
    def auth_user():
        users = Fullname.query.all()

        response = check_image(fingerprint, users)
        if response:
            log_entry = FullnameLog(name=response)
            db.session.add(log_entry)
            db.session.commit()
            flash(f'Пользователь {response} успешно авторизован!')

            return redirect(url_for('access_page'))
        
        flash('Отпечаток пальца не найден.')

        return redirect(url_for('access_page'))


    @app.route('/auth_user_test', methods=['POST'])
    def auth_user_test():
        users = Fullname.query.all()

        response = check_image(fingerprint, users)
        if response:
            flash(f'Пользователь {response} найден!')

            return redirect(url_for('auth_management'))
        
        flash('Отпечаток пальца не найден. Попробуйте снова.')

        return redirect(url_for('auth_management'))


    @app.route('/login', methods=['GET', 'POST'])
    def login_page():
        login = request.form.get('login')
        password = request.form.get('password')

        if login and password:
            user = User.query.filter_by(login=login).first()

            if user and check_password_hash(user.password, password):
                login_user(user)

                return redirect(url_for('auth_management'))
            else:
                flash('Неправильное имя пользователя или пароль!')

        return render_template('login.html')


    @app.route('/register', methods=['GET', 'POST'])
    @login_required
    def register():
        login = request.form.get('login')
        password = request.form.get('password')
        password2 = request.form.get('password2')

        if request.method == 'POST':
            if not (login or password or password2):
                flash('Заполните все поля!')
            elif password != password2:
                flash('Пароли не совпадаюст!')
            else:
                hash_pwd = generate_password_hash(password)
                new_user = User(login=login, password=hash_pwd)
                db.session.add(new_user)
                db.session.commit()

                return redirect(url_for('user_management'))

        return render_template('register.html')


    @app.route('/change_password', methods=['GET', 'POST'])
    @login_required
    def change_password():
        login = request.form.get('login')
        password = request.form.get('password')
        password2 = request.form.get('password2')

        if request.method == 'POST':
            required_login = db.session.query(User).filter_by(login=login).first()
            if not (login or password or password2):
                flash('Заполните все поля!')
            elif password != password2:
                flash('Пароли не совпадают!')
            elif required_login and password and password2:          
                hash_pwd = generate_password_hash(password)
                required_login.password = hash_pwd
                db.session.commit()
                flash('Пароль успешно обновлен!')
        
        return render_template('change_password.html')        


    @app.route('/logout', methods=['GET', 'POST'])
    @login_required
    def logout():
        logout_user()
        return redirect(url_for('root_page'))


    @app.after_request
    def redirect_to_signin(response):
        if response.status_code == 401:
            return redirect(url_for('login_page') + '?next=' + request.url)

        return response
