from datetime import datetime
from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Fullname(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now)

    def __init__(self, name, images):
        
        self.name = name
        self.images = [
            Image(image=images)
        ]


class FullnameLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    auth_at = db.Column(db.DateTime, default=datetime.now)


class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.JSON, nullable=False)

    fullname_id = db.Column(db.Integer, db.ForeignKey('fullname.id'), nullable=False)
    fullname = db.relationship('Fullname', backref=db.backref('images', lazy=True))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(128), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)
