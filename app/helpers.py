import json
from pyfingerprint.pyfingerprint import PyFingerprint, FINGERPRINT_CHARBUFFER1, FINGERPRINT_CHARBUFFER2
from models import Fullname


def check_image(fingerprint: PyFingerprint, users: Fullname) -> (str | None): 

    while not fingerprint.readImage():
        pass

    fingerprint.convertImage(FINGERPRINT_CHARBUFFER1)
    current_characteristic = fingerprint.downloadCharacteristics(FINGERPRINT_CHARBUFFER1)

    for user in users:
        for image_obj in user.images:  
            fingerprint_data = json.loads(image_obj.image)

            for fingerprint_characteristic in fingerprint_data:
                fingerprint.uploadCharacteristics(FINGERPRINT_CHARBUFFER1, current_characteristic)
                fingerprint.uploadCharacteristics(FINGERPRINT_CHARBUFFER2, fingerprint_characteristic)

                match = fingerprint.compareCharacteristics()

                if match:
                    return user.name

    return None
